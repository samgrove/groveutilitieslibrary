package com.grove.NumberFormatter;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class NumberFormatTest {

    NumberFormat nf;

    @Before
    public void setUp() throws Exception {
        nf = new NumberFormat();
    }

    /**
     * Tests double input
     */
    @Test
    public void formatIncome() {
        assertEquals("9,999.9", nf.formatIncome(9999.9));
        assertEquals("9,999.99", nf.formatIncome(9999.99));
        assertEquals("99,999.99", nf.formatIncome(99999.99));
        assertEquals("999,999.99", nf.formatIncome(999999.99));
        assertEquals("9,999,999.99", nf.formatIncome(9999999.99));
        assertEquals("9", nf.formatIncome(9.0));
        assertEquals("9", nf.formatIncome(9.00));
    }

    /**
     * Tests int input
     */
    @Test
    public void testFormatIncome() {
        assertEquals("1", nf.formatIncome(1));
        assertEquals("0", nf.formatIncome(0));
        assertEquals("99", nf.formatIncome(99));
        assertEquals("999", nf.formatIncome(999));
        assertEquals("9,999", nf.formatIncome(9999));
        assertEquals("99,999", nf.formatIncome(99999));
        assertEquals("999,999", nf.formatIncome(999999));
        assertEquals("9,999,999", nf.formatIncome(9999999));
        assertEquals("99,999,999", nf.formatIncome(99999999));
        assertEquals("999,999,999", nf.formatIncome(999999999));

    }

    @Test
    public void testFormatIncome1() {
        assertEquals("1", nf.formatIncome("1"));
        assertEquals("0", nf.formatIncome("0"));
        assertEquals("99", nf.formatIncome("99"));
        assertEquals("999", nf.formatIncome("999"));
        assertEquals("9,999", nf.formatIncome("9999"));
        assertEquals("99,999", nf.formatIncome("99999"));
        assertEquals("999,999", nf.formatIncome("999999"));
        assertEquals("9,999,999", nf.formatIncome("9999999"));
        assertEquals("99,999,999", nf.formatIncome("99999999"));
        assertEquals("999,999,999", nf.formatIncome("999999999"));
    }

    @Test
    public void formatRawDate() {
        assertEquals("13 February 1993", nf.formatRawDate("1993-02-13"));
        assertEquals("33 January 2020", nf.formatRawDate("2020-01-33"));
        assertEquals("22 November 2019", nf.formatRawDate("2019-11-22"));
        assertEquals("1 December 2200", nf.formatRawDate("2200-12-01"));
        assertEquals("31 October 2022", nf.formatRawDate("2022-10-31"));
        assertEquals("Date invalid", nf.formatRawDate("2020-00-33"));
    }
}