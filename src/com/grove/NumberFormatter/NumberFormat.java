package com.grove.NumberFormatter;

import java.text.DecimalFormat;

public class NumberFormat {

    DecimalFormat formatter = new DecimalFormat("#,###.##");

    /**
     * Takes a double and returns a string in the standard Grove currency format. #,###.##
     * Does not include currency signs or iso currency code.
     * 9999.99 = 9,999.99
     * 9.0 = 9
     * @param income double input
     * @return string
     */
    public String formatIncome(double income) {
        return formatIncomeString(income);
    }

    /**
     *  Takes an integer and returns a string in the standard Grove currency format. #,###.##
     *  Does not include currency signs or iso currency code.
     *       * 9999 = 9,999
     *       * 9 = 9
     * @param income int ivnput
     * @return string
     */
    public String formatIncome(int income) {
        return formatIncomeString(income);
    }

    /**
     * Takes a string and returns a string in the standard Grove currency format. #,###.##
     * Does not include currency signs or iso currency code.
     * "9999.99" = 9,999.99
     * "9.0" = 9
     * @param income
     * @return Standarised String
     */
    public String formatIncome(String income) {
        return formatIncomeString(Double.parseDouble(income));
    }

    /**
     * Takes a double and turns into a String into a standardised string format.
     * @param string
     * @return Standardised String
     */
    private String formatIncomeString(double string) {
        try {
            String incomeString = formatter.format(string);

        if (incomeString.length() == 1) {
            return incomeString;
        }
        if (incomeString.charAt(incomeString.length() - 2) == '.' && incomeString.charAt(incomeString.length() - 1) == '0') {
            String[] rentSplit = incomeString.split("\\.");
            return rentSplit[0];
        } else {
            return incomeString;
        }
        } catch (Exception e) {
            return "0";
        }
    }


    /**
     * Takes a date in the format of YEAR-MO-DA, eg. 1993-02-13 will return 13 February 1993
     * @param rawDate
     * @return Formated String
     */
    public String formatRawDate(String rawDate) {
        String[] months = {"January", "February", "March", "April", "May", "June", "July",
                "August", "September", "October", "November", "December"};

        String year = "";
        String month = "";
        String day = "";

        char[] yearArray = new char[4];
        char[] dayArray = new char[2];
        char[] monthArray = new char[2];

        try {
            rawDate.getChars(0, 4, yearArray, 0);
            for (int i = 0; i < 4; i++) {
                year = year + yearArray[i];
            }
            System.out.println(year);

            rawDate.getChars(5, 7, monthArray, 0);
            if (monthArray[0] == '0') {
                month = month + monthArray[1];
            } else {
                for (int i = 0; i < 2; i++) {
                    month = month + monthArray[i];
                }
            }
            System.out.println(month);
            System.out.println(months[Integer.parseInt(month) - 1]);
            month = months[Integer.parseInt(month) - 1];

            rawDate.getChars(8, 10, dayArray, 0);
            if (dayArray[0] == '0') {
                day = day + dayArray[1];

            } else {
                for (int i = 0; i < 2; i++) {
                    day = day + dayArray[i];
                }
            }

            return day + " " + month + " " + year;
        } catch (Exception e) {
            System.out.println(e);
            return "Date invalid";
        }
    }
}
